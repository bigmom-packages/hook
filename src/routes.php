<?php

use Bigmom\Auth\Http\Middleware\Authenticate;
use Bigmom\Hook\Http\Controllers\HookController;
use Bigmom\Auth\Http\Middleware\EnsureUserIsAuthorized;
use Illuminate\Support\Facades\Route;

Route::prefix('bigmom/hook')->name('bigmom-hook.')->middleware(['web'])->group(function () {
    Route::middleware([Authenticate::class, EnsureUserIsAuthorized::class.':hook-manage'])->group(function () {
        Route::get('/', [HookController::class, 'getIndex'])->name('getIndex');
        Route::post('/create', [HookController::class, 'postCreate'])->name('postCreate');
        Route::post('/update', [HookController::class, 'postUpdate'])->name('postUpdate');
        Route::post('/delete', [HookController::class, 'postDelete'])->name('postDelete');
        Route::post('/clear-cache', [HookController::class, 'postClearCache'])->name('postClearCache');
    });
});
