<?php

namespace Bigmom\Hook\Services;

use Bigmom\Hook\Models\Hook;
use Bigmom\Hook\Traits\ResolveCacheKey;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

class HookService
{
    use ResolveCacheKey;

    protected $useCache = true;

    public function noCache()
    {
        $this->useCache = false;
        return $this;
    }

    public function get($key, $default = null)
    {
        $model = $this->getModel($key);
        return $model ? $model->value : $default;
    }

    public function getExtra($key, $default = null)
    {
        $model = $this->getModel($key);
        return $model ? $model->extra : $default;
    }

    public function getModel($key)
    {
        if ($this->useCache) {
            return $this->cacheGetModel($key);
        } else {
            $this->useCache = true;
            return $this->noCacheGetModel($key);
        }
    }

    public function cacheGetModel($key)
    {
        $result = Cache::rememberForever($this->resolveHookCacheKey($key), function () use ($key) {
            return $this->noCacheGetModel($key);
        });
        // if (!$result) {
        //     Cache::forget($this->resolveHookCacheKey($key));
        // }
        return $result;
    }

    public function noCacheGetModel($key)
    {
        return Hook::where('key', $key)->first();
    }

    public function set(array $input)
    {
        Validator::make($input, [
            'key' => 'required|string|max:191',
            'value' => 'required|string|max:191',
            'extra' => 'nullable|string|max:65535',
            'hook-id' => 'nullable|exists:hooks,id',
        ])->validate();
        
        if (isset($input['hook-id'])) {
            $hook = Hook::find($input['hook-id']);
        } else if ($this->get($input['key'])) {
            $hook = $this->getModel($input['key']);
        } else {
            $hook = new Hook;
        }
        $hook->clearCache();
        $hook->key = $input['key'];
        $hook->value = $input['value'];
        $hook->extra = isset($input['extra'])
            ? $input['extra']
            : null;
        $hook->save();

        return $hook;
    }
}