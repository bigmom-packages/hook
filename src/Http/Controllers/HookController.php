<?php

namespace Bigmom\Hook\Http\Controllers;

use Bigmom\Hook\Facades\Hook as FacadesHook;
use Bigmom\Hook\Models\Hook;
use Illuminate\Http\Request;

class HookController extends Controller
{
    public function getIndex(Request $request)
    {
        $request->validate([
            'search' => 'nullable|string|max:191',
        ]);

        $hooks = Hook::when($request->input('search'), function ($query, $search) {
            return $query->where('key', 'like', "%$search%")
                ->orWhere('value', 'like', "%$search%")
                ->orWhere('extra', 'like', "%$search%");
        })->paginate(15);
        return view('bigmom-hook::hook.index', compact('hooks'));
    }

    public function postCreate(Request $request)
    {
        FacadesHook::set($request->input());

        return redirect()
            ->back()
            ->with('success', 'Hook successfully created.');
    }

    public function postUpdate(Request $request)
    {
        FacadesHook::set($request->input());

        return redirect()
            ->back()
            ->with('success', 'Hook successfully updated.');
    }

    public function postDelete(Request $request)
    {
        $request->validate([
            'hook-id' => 'required|integer|exists:hooks,id',
        ]);

        Hook::find($request->input('hook-id'))->clearCache()->delete();

        return redirect()
            ->back()
            ->with('success', 'Hook successfully deleted.');
    }

    public function postClearCache(Request $request)
    {
        $request->validate([
            'hook-id' => 'required|integer|exists:hooks,id',
        ]);

        Hook::find($request->input('hook-id'))->clearCache();

        return redirect()
            ->back()
            ->with('success', 'Hook cache successfully cleared.');
    }
}
