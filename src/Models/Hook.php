<?php

namespace Bigmom\Hook\Models;

use Bigmom\Hook\Traits\ResolveCacheKey;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Hook extends Model
{
    use HasFactory, ResolveCacheKey;

    public function getCachedValueAttribute()
    {
        $result = $this->cached_model;

        return $result ? $result->value : null;
    }

    public function getCachedExtraAttribute()
    {
        $result = $this->cached_model;

        return $result ? $result->extra : null;
    }

    public function getCachedModelAttribute()
    {
        return Cache::get($this->resolveHookCacheKey($this->key));
    }

    public function clearCache()
    {
        Cache::forget($this->resolveHookCacheKey($this->key));
        return $this;
    }
}
