<?php

namespace Bigmom\Hook\Traits;

trait ResolveCacheKey
{
    protected function resolveHookCacheKey($key)
    {
        return $this->resolveCacheKey($key, 'hook');
    }

    protected function resolveCacheKey($key, $prefix)
    {
        return $prefix . '_' . $key;
    }
}
