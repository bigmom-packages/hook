<x-bigmom-auth::layout.main>
  <x-slot name="header">Hook</x-slot>
  <x-slot name="headerRightSide">
    <a href="{{ route('bigmom-auth.getHome') }}" class="bg-blue-600 hover:bg-blue-800 text-white font-bold px-4 py-2 rounded">Home</a> 
  </x-slot>

  <x-bigmom-auth::card class="pt-8">
    <div x-data="{ showModal: false, showCreateModal: false, showUpdateModal: false, showDeleteModal: false, showClearCacheModal: false, key: '', value: '', extra: '', hookId: '' }">
      <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
          Hook - {{ config('app.name', 'Laravel') }}
        </h2>
      </x-slot>

      <form action="{{ route('bigmom-hook.getIndex') }}" class="w-full flex" method="GET">
        @csrf
        <input type="text" class="flex-1 my-2 mx-2 flex-1 shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="search">
        <button type="submit" class="cursor-pointer mx-2 my-2 w-auto bg-gray-300 hover:bg-gray-500 text-black font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Search</button>
      </form>
      <button class="cursor-pointer mx-2 my-2 w-auto bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" @click="showModal = true; showCreateModal = true;">Create Hook</button>
        <table class="table-auto w-full">
          <thead>
            <tr>
              <th class="px-4 py-2 w-auto">Key</th>
              <th class="px-4 py-2 w-auto">Value</th>
              <th class="px-4 py-2 w-auto">Extra</th>
              <th class="px-4 py-2 w-auto">Action</th>
            </tr>
          </thead>
          <tbody>
            @forelse($hooks as $hook)
            <tr>
              <td class="border px-4 py-2">{{ $hook->key }}</td>
              <td class="border px-4 py-2">{{ $hook->value }} (Cached: {{ $hook->cached_value ?? '[N/A]' }})</td>
              <td class="border px-4 py-2">{{ $hook->extra ?? '[N/A]' }} (Cached: {{ $hook->cached_extra ?? '[N/A]' }})</td>
              <td class="border px-4 py-2">
                <button @click="showModal = true; showUpdateModal = true; key = '{{ $hook->key }}'; value = '{{ $hook->value }}'; extra = '{{ $hook->extra }}'; hookId = {{ $hook->id }}" class="mx-2 w-auto bg-yellow-300 hover:bg-yellow-500 text-black font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Update</button>
                <button @click="showModal = true; showDeleteModal = true; key = '{{ $hook->key }}'; hookId = {{ $hook->id }}" class="mx-2 w-auto bg-red-600 hover:bg-red-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Delete</button>
                <button @click="showModal = true; showClearCacheModal = true; key = '{{ $hook->key }}'; hookId = {{ $hook->id }}" class="mx-2 w-auto bg-gray-300 hover:bg-gray-500 text-black font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Clear Cache</button>
              </td>
            </tr>
            @empty
            <tr>
              <td class="border px-4 py-2" colspan="4">No hooks yet.</td>
            </tr>
            @endforelse
          </tbody>
        </table>
        <div class="py-3 px-3">
          {{ $hooks->withQueryString()->links() }}
        </div>
      
          <div class="fixed w-screen h-screen left-0 flex justify-center top-0 items-center bg-gray-500 bg-opacity-25" x-show="showModal">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
              <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" @click.away="showClearCacheModal = false; showDeleteModal = false; showUpdateModal = false; showCreateModal = false; showModal = false">
                <form class="px-4 py-2 text-center container" x-show="showCreateModal" action="{{ route('bigmom-hook.postCreate') }}" method="POST">
                  @csrf
                  <div class="flex-1 flex items-center my-2">
                    <label for="create-key" class="w-28">Key</label>
                    <input type="text" id="create-key" name="key" class="mx-4 flex-1 shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                  </div>
                  <div class="flex-1 flex items-center my-2">
                    <label for="create-value" class="w-28">Value</label>
                    <input type="text" id="create-value" name="value" class="mx-4 flex-1 shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                  </div>
                  <div class="flex-1 flex items-center my-2">
                    <label for="create-extra" class="w-28">Extra (Optional)</label>
                    <textarea id="create-extra" name="extra" class="mx-4 flex-1 shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"></textarea>
                  </div>
                  <input type="submit" value="Create" class="cursor-pointer mx-2 w-auto bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                </form>
                <form class="px-4 py-2 text-center container" x-show="showUpdateModal" action="{{ route('bigmom-hook.postUpdate') }}" method="POST">
                  @csrf
                  <div class="flex-1 flex items-center my-2">
                    <label for="update-key" class="w-28">Key</label>
                    <input type="text" id="update-key" name="key" class="mx-4 flex-1 shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" x-model="key">
                  </div>
                  <div class="flex-1 flex items-center my-2">
                    <label for="update-value" class="w-28">Value</label>
                    <input type="text" id="update-value" name="value" class="mx-4 flex-1 shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" x-model="value">
                  </div>
                  <div class="flex-1 flex items-center my-2">
                    <label for="update-extra" class="w-28">Extra (Optional)</label>
                    <textarea id="update-extra" name="extra" class="mx-4 flex-1 shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" x-model="extra"></textarea>
                  </div>
                  <input type="hidden" name="hook-id" x-model="hookId">
                  <input type="submit" value="Update" class="cursor-pointer mx-2 w-auto bg-yellow-300 hover:bg-yellow-500 text-black font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                </form>
                <form class="px-4 py-2 text-center container" x-show="showDeleteModal" action="{{ route('bigmom-hook.postDelete') }}" method="POST">
                  @csrf
                  <p>Are you sure you want to delete this hook?</p>
                  <input type="text" class="mx-4 flex-1 shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" x-model="key" disabled>
                  <input type="hidden" name="hook-id" x-model="hookId">
                  <input type="submit" value="Delete" class="cursor-pointer mx-2 w-auto bg-red-600 hover:bg-red-800 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                </form>
                <form class="px-4 py-2 text-center container" x-show="showClearCacheModal" action="{{ route('bigmom-hook.postClearCache') }}" method="POST">
                  @csrf
                  <p>Are you sure you want to clear this hook's cache?</p>
                  <input type="text" class="mx-4 flex-1 shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" x-model="key" disabled>
                  <input type="hidden" name="hook-id" x-model="hookId">
                  <input type="submit" value="Clear Cache" class="cursor-pointer mx-2 w-auto bg-gray-300 hover:bg-gray-500 text-black font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
                </form>
              </div>
            </div>
          </div>
        </div>
    </x-bigmom-auth::card>
      @push('script')
      <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.0/dist/alpine.min.js" defer></script>
      @endpush
</x-bigmom-auth::layout.main>