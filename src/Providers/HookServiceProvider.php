<?php

namespace Bigmom\Hook\Providers;

use Bigmom\Hook\Services\HookService;
use Bigmom\Hook\Facades\Hook;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class HookServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        config([
            'bigmom-auth.packages' => array_merge([[
                'name' => 'Hook',
                'description' => 'Simple key-value pair manager.',
                'routes' => [
                    [
                        'title' => 'Index',
                        'name' => 'bigmom-hook.getIndex',
                        'permission' => 'hook-manage',
                    ]
                ],
                'permissions' => [
                    'hook-manage',
                ]
            ]], config('bigmom-auth.packages', []))
        ]);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../public' => public_path('vendor/bigmom-hook'),
            ], 'public');
        }

        $this->loadRoutesFrom(__DIR__.'/../routes.php');

        $this->loadMigrationsFrom(__DIR__.'/../migrations');

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'bigmom-hook');

        $this->app->singleton('bigmom-hook', function ($app) {
            return new HookService;
        });
    }
}
