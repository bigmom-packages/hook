# Bigmom Hook

A simple key-value manager with a field for extra stuff, all stored in database.

## Installation

- `composer require bigmom/hook`
- `php artisan vendor:publish`
- `php artisan migrate`

If [bigmom/auth](https://packagist.org/packages/bigmom/auth) has not been published yet, please do so.

## Usage

### Management

Access the management UI through `/bigmom`. Create hooks first before using them.

### Facade

`use Bigmom\Hook\Facades\Hook`

Methods:
- `noCache()` - By default, hook models are cached. If you do not want to use the cached value, call this function method before using get functions. i.e. `Hook::noCache()->get('hook');`
- `get($key, $default = null)` - Returns the value of the hook model with the key specified by $key. If not exist, return default value as specified by $default.
- `getExtra($key, $default = null)` - Returns the extra of the hook model with the key specified by $key. If not exist, return default extra as specified by $default.
- `getModel($key)` - If for whatever reason model is needed, use this method.